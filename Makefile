ifndef ROOTSYS
$(error ROOT is missing)
endif

ROOTCFLAGS    = $(shell $(ROOTSYS)/bin/root-config  --cflags)
ROOTLIBS      = $(shell $(ROOTSYS)/bin/root-config --libs)
ROOTGLIBS     = $(shell $(ROOTSYS)/bin/root-config --glibs)

CXX           = g++

NGLIBS        = $(ROOTGLIBS)
NGLIBS       += -lMinuit

CXXFLAGS      = $(ROOTCFLAGS)
CXX          += -I./include
LIBS          = $(ROOTLIBS)

GLIBS         = $(filter-out -lNew, $(NGLIBS))

CXX          += -I./obj/
OUTLIB	      = ./obj/

#----------------------------------------------------#

all : occupancy

occupancy : main.cpp obj/occupancy.o
	$(CXX) $(CXXFLAGS) -o occupancy $(OUTLIB)/*.o $(GLIBS) $<

obj/occupancy.o : src/occupancy.cpp include/occupancy.hpp
	$(CXX) $(CXXFLAGS) -c -I. -o $(OUTLIB)occupancy.o $<

.PHONY : clean
clean :
	rm -f *~
	rm -f occupancy
	rm -rf occupancy.dSYM
	rm -f obj/*
	rm -f result/*
	# rm -f data/*_out.root
