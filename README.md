# occupancy

----------------------------------------------------------------------

## Mighty Tracker IT/MT occupancy calculations for the various geometries

- make clean && make

- ./occupancy [-h/--help] : Use to print usage information

- ./occupancy [input file] [Scenario 1] [Scenario 2] ... [Scenarion X] : Analyse data file for the specified Scenario(s)

                input file: file that is in data/
                            data/[input file].root
                            
                Scenario: MT geometry scenarios [1 - 4] (check /config)

                Examples:
                    - ./occupancy incl_b/MCtracks_2e33 2 3
                    - ./occupancy 2e34 4

## Data files
https://cernbox.cern.ch/index.php/s/SYi3ITkZt8KJUIZ

## Requirements
- ROOT (tested with v6.22.08)
- g++ (c++11 minimum)
