double calculations(TH1F * _h);

void average_distr() {
	TFile * in_file  = new TFile("../data/15e33-no-foil.root", "READ");
	TTree * track = (TTree * )in_file->Get("Hits");

	double x_range_min_temp = 134.25;
	double x_range_max_temp = 135.75;

	double x, y, e;
	track->SetBranchAddress("x", &x);
	track->SetBranchAddress("y", &y);
	track->SetBranchAddress("e", &e);
	long int n_entries = track->GetEntries();

	vector<float> unique_events;

	for(long long int j = 0; j < n_entries; ++j) {
		track->GetEntry(j);
        if(find(unique_events.begin(), unique_events.end(), e) == unique_events.end()) {
			unique_events.push_back(e);
            cout << e << endl;
        }
	}

	int x_bin_fiber_temp = 2 * x_range_max_temp / 0.25;

	int number_of_unique_events = unique_events.size();

	TH1F * h_temp[number_of_unique_events];

	for(int i = 0; i < number_of_unique_events; ++i)
		h_temp[i] = new TH1F("", "", x_bin_fiber_temp, -x_range_max_temp, x_range_max_temp);

	for(int j = 0; j < n_entries; ++j) {
		track->GetEntry(j);
		if((x >= -x_range_max_temp && x <= -x_range_min_temp) || (x >= x_range_min_temp && x <= x_range_max_temp))
			for (int k = 0; k < number_of_unique_events; ++k)
				if(e == unique_events[k]) {
					h_temp[k]->Fill(x);
					break;
				}
		break;
	}

	TFile * out_file = new TFile("average_distr.root", "RECREATE");

	TNtuple * average_ntuple = new TNtuple("Additional", "Average occupancy per fiber", "averageOccupancy");

	for(int i = 0; i < number_of_unique_events; ++i) {
		double tmp_aver_occ = calculations(h_temp[i]);
		// if(tmp_aver_occ != 0 && tmp_aver_occ <= 1.)
		average_ntuple->Fill(tmp_aver_occ);
		delete h_temp[i];
	}
	average_ntuple->Write();
	out_file->Close();
}

double calculations(TH1F * _h) {
	int number_of_bins = 12;

	_h->Scale(0.5);

	return _h->Integral()/(number_of_bins);
}
