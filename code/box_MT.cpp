void box_MT() {
	TBox * b[4];

	b[0] = new TBox(-530, -500, 530, 500);
	b[1] = new TBox(-1060, -400, 1060, 400);
	b[2] = new TBox(-1590, -300, 1590, 300);
	b[3] = new TBox(-2120, -200, 2120, 200);

	for(int i = 0; i < 4; ++i) {
		b[i]->SetFillColor(kWhite);
		b[i]->Draw();
	}
}
