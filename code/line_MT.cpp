void line_MT() {
	vector<TLine *> l;
	l.push_back(new TLine(-530, -500, 530, -500));
	l.push_back(new TLine(-530, 500, 530, 500));
	l.push_back(new TLine(-1060, -400, -530, -400));
	l.push_back(new TLine(-1060, 400, -530, 400));
	l.push_back(new TLine(530, -400, 1060, -400));
	l.push_back(new TLine(530, 400, 1060, 400));
	l.push_back(new TLine(-1590, -300, -1060, -300));
	l.push_back(new TLine(-1590, 300, -1060, 300));
	l.push_back(new TLine(1060, -300, 1590, -300));
	l.push_back(new TLine(1060, 300, 1590, 300));
	l.push_back(new TLine(-2120, -200, -1590, -200));
	l.push_back(new TLine(-2120, 200, -1590, 200));
	l.push_back(new TLine(1590, -200, 2120, -200));
	l.push_back(new TLine(1590, 200, 2120, 200));
	l.push_back(new TLine(2120, 200, 2120, -200));
	l.push_back(new TLine(-2120, 200, -2120, -200));

	for(auto i = begin(l); i != end(l); ++i) {
		(*i)->SetLineColor(kWhite);
		(*i)->SetLineWidth(3);
		(*i)->Draw();
	}
}
