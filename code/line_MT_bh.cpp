void line_MT_bh() {
	vector<TLine *> l;
	l.push_back(new TLine(-530, -500, 530, -500));
	l.push_back(new TLine(-530, 500, 530, 500));
	l.push_back(new TLine(-1060, -400, -530, -400));
	l.push_back(new TLine(-1060, 400, -530, 400));
	l.push_back(new TLine(530, -400, 1060, -400));
	l.push_back(new TLine(530, 400, 1060, 400));
	l.push_back(new TLine(-1590, -300, -1060, -300));
	l.push_back(new TLine(-1590, 300, -1060, 300));
	l.push_back(new TLine(1060, -300, 1590, -300));
	l.push_back(new TLine(1060, 300, 1590, 300));
	l.push_back(new TLine(-2120, -200, -1590, -200));
	l.push_back(new TLine(-2120, 200, -1590, 200));
	l.push_back(new TLine(1590, -200, 2120, -200));
	l.push_back(new TLine(1590, 200, 2120, 200));
	l.push_back(new TLine(2120, 200, 2120, -200));
	l.push_back(new TLine(-2120, 200, -2120, -200));

	for(auto i = begin(l); i != end(l); ++i) {
		(*i)->SetLineColor(kRed);
		(*i)->SetLineWidth(3);
		(*i)->Draw();
	}

	TBox * b = new TBox(-530, -300, 530, 300);
	b->SetFillColor(kWhite);
	b->Draw();
}
