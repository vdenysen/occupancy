// Include files
#include "Event/Track.h"
#include "Event/MCParticle.h"
#include "Event/MCHit.h"
#include "Event/ODIN.h"

#include "Linker/LinkedTo.h"
#include "Linker/LinkerWithKey.h"
#include "Linker/LinkedFrom.h"

#include "MCHitsNTuple.h"
#include "TMath.h"

using namespace std;

//-----------------------------------------------------------------------------
// Implementation file for class : MCHitsNTuple
//
// 2018-06-01 : Lucia Grillo (starting from Olivier Callot PatCheckerNTuple)
// 2019-01-09 : Vadym Denysenko (starting from Lucia Grillo MCParticleNTuple
//								 + Laurent Dufour MCParticleTuple)
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT(MCHitsNTuple)

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCHitsNTuple::MCHitsNTuple(const std::string& name, ISvcLocator* pSvcLocator)
 : GaudiTupleAlg(name, pSvcLocator), n_events(0)
{
	declareProperty("NtupleName", m_tupleName = "Tracks");
	declareProperty("OutputData", m_outputData = "MC/Particles2MCTrackerHits");
	declareProperty("Detectors", m_dets = {"VP", "UT", "FT"});
}

//=============================================================================
// Destructor
//=============================================================================
MCHitsNTuple::~MCHitsNTuple() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MCHitsNTuple::initialize()
{
	out_txt_file.open("unique_events.txt", ofstream::out | ofstream::app);

	StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
	if(sc.isFailure())
		return sc;  // error printed already by GaudiAlgorithm

	debug() << "==> Initialize" << endmsg;

	return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MCHitsNTuple::execute()
{
	debug() << "==> Execute" << endmsg;

	const LHCb::ODIN* odin = getIfExists<LHCb::ODIN>(LHCb::ODINLocation::Default);

	if(!odin)
		return StatusCode::SUCCESS;

	for(auto det:m_dets)
	{
		// get MCHits
		LHCb::MCHits* mcHits = get <LHCb::MCHits> ("/Event/MC/" + det + "/Hits");

		if(det.compare("VP") == 0)
			++n_events;

		vector<pair<int, double>> unique_hits;

		for(LHCb::MCHits::const_iterator itHit = mcHits->begin(); itHit != mcHits->end(); ++itHit)
		{
			LHCb::MCHit* mcHit = *itHit;

			if(mcHit)
			{
				debug() << "Filling Tuple" << endmsg;

				float z = mcHit->mcParticle()->originVertex()->position().z();
				if(z < -100. or z > 500.)
					continue;
				double eta = TMath::ATanH(mcHit->mcParticle()->momentum().Pz()/mcHit->mcParticle()->momentum().P());
				if(eta < 2. or eta > 5.)
					continue;

				// Fill the ntuple
				if(mcHit->entry().z() > 7810 and mcHit->entry().z() < 7840)
				{
					if(find(unique_hits.begin(), unique_hits.end(), make_pair(mcHit->sensDetID(), mcHit->mcParticle()->pt())) == unique_hits.end())
					{
						unique_hits.push_back(make_pair(mcHit->sensDetID(), mcHit->mcParticle()->pt()));
						Tuple tPoint = nTuple(m_tupleName);
						tPoint->column("HitXpos", mcHit->entry().x());
						tPoint->column("HitYpos", mcHit->entry().y());
						tPoint->column("HitZpos", mcHit->entry().z());
						tPoint->column("eventNumber", odin->eventNumber());
						tPoint->column("runNumber", odin->runNumber());
						tPoint->write();
					}
				}
				debug() << "DONE Filling Tuple" << endmsg;
			}
			else
				return Error("Failed retrieving MCHit");
		}
	}

	return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode MCHitsNTuple::finalize()
{
	out_txt_file << n_events;
	out_txt_file.close();

	debug() << "==> Finalize" << endmsg;

	return GaudiTupleAlg::finalize();  // must be called after all other actions
}
//=============================================================================

//  LocalWords:  endmsg
