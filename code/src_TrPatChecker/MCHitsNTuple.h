// $Id: PatCheckerNTuple.h,v 1.1.1.1 2007-10-09 18:41:19 smenzeme Exp $
#ifndef MCHITSNTUPLE_H
#define MCHITSNTUPLE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>


/** @class MCHitsNTuple MCHitsNTuple.h
 *
 *   Algorithm to produce an NTuple with hits information
 *  @update Vadym Denysenko (study of MCHits (occupancy))
 */


class MCHitsNTuple : public GaudiTupleAlg
{
	public:
		MCHitsNTuple(const std::string& name, ISvcLocator* pSvcLocator);
		virtual ~MCHitsNTuple(); ///< Destructor

		StatusCode initialize() override;    ///< Algorithm initialization
		StatusCode execute() override;    ///< Algorithm execution
		StatusCode finalize() override;    ///< Algorithm finalization

	private:
		std::vector<std::string> m_dets;
		std::string m_outputData;
		std::string m_inputData;
		std::string m_tupleName;

		std::ofstream out_txt_file;
		unsigned int n_events;
};

#endif // MCHITSNTUPLE_H
