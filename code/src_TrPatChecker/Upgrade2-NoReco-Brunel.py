###############################################################################
# File for running alignment with D0 reconstructed from HLT sel reports
###############################################################################
# Syntax is:
#   gaudirun.py Upgrade2-NoReco-Brunel.py
###############################################################################

import os, sys
os.environ['NO_GIT_CONDDB'] = '1'

from Configurables import Brunel, LHCbApp, CondDB, NTupleSvc, DigiConf
from Gaudi.Configuration import *

# Just instantiate the configurable...
theApp = Brunel()
theApp.DataType = "Upgrade"
theApp.InputType = "DIGI"
DigiConf().DigiType = "Extended"
theApp.WithMC = True
theApp.Simulation = True
theApp.Detectors  = ['VP', 'UT', 'FT', 'Magnet', 'Tr']
theApp.RecoSequence = []
theApp.Monitors = []
theApp.PrintFreq = -1
theApp.EvtMax = -1
theApp.MCLinksSequence = ["Unpack", "Tr"]
theApp.OutputType = 'ROOT'
theApp.DatasetName = 'MC-NoRecoTest'

CondDB().Upgrade = True
theApp.DDDBtag = "dddb-20170726"
theApp.CondDBtag = "sim-20170301-vc-mu100"

from Configurables import CondDBAccessSvc, CondDB, UpdateManagerSvc, L0Conf, OutputStream, NTupleSvc
from Gaudi.Configuration import appendPostConfigAction
CondDB().UseOracle = False
CondDB().IgnoreHeartBeat= True
CondDB().EnableRunStampCheck = False

L0Conf().EnsureKnownTCK=False

from Configurables import GaudiSequencer, MCHitsNTuple

def setup_mc_truth_matching():
    GaudiSequencer("CheckPatSeq").Members = [MCHitsNTuple("MCHitsNTuple")]
    NTupleSvc().Output = ["FILE1 DATAFILE='MCtracks.root' TYP='ROOT' OPT='NEW'"]
    GaudiSequencer('MoniTrSeq').Members = []
    GaudiSequencer('MCLinksTrSeq').Members = []

appendPostConfigAction(setup_mc_truth_matching)

OutputStream("DstWriter").ItemList

#Digi from Greg
from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
	#'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/October2018/2e34/incl_b/Boole-Extended_0.digi',
	#'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/October2018/2e34/incl_b/Boole-Extended_1.digi',
	#'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/October2018/2e34/incl_b/Boole-Extended_2.digi',
	#'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/October2018/2e34/incl_b/Boole-Extended_3.digi',
	#'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/October2018/2e34/incl_b/Boole-Extended_4.digi'

	#'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/Example_current_upgrade_13104012_Upgrade_MagDown.xdigi',
	#'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/00067195_00000017_1.xdigi',
	#'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/00067195_00000018_1.xdigi',
	#'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/00067195_00000034_1.xdigi'

	#'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/October2018/2e34/minibias/Boole-Extended_0.digi',
	#'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/October2018/2e34/minibias/Boole-Extended_1.digi',
    #'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/October2018/2e34/minibias/Boole-Extended_2.digi',
    #'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/October2018/2e34/minibias/Boole-Extended_3.digi',
    #'/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/October2018/2e34/minibias/Boole-Extended_4.digi'

    '/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/nobias_files_Upgrade1/00067189_00000001_1.xdigi',
    '/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/nobias_files_Upgrade1/00067195_00000005_1.xdigi'
], clear=True)
