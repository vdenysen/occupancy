import sys
import GaudiPython as GP
import ROOT

from GaudiConf import IOHelper
from Configurables import DaVinci

theApp = DaVinci()
theApp.DataType   = "Upgrade"
theApp.InputType  = "DIGI"
theApp.Simulation = True
theApp.Detectors  = ['FT']
theApp.PrintFreq = 100
theApp.EvtMax = -1

from Configurables import CondDB
CondDB().Upgrade = True

from Configurables import LHCbApp
# theApp.CondDBtag = "sim-20180530-vc-mu100"
# theApp.DDDBtag = "dddb-20180815"
theApp.CondDBtag = "sim-20171127-vc-md100"
theApp.DDDBtag = "dddb-20171126"

""" Disable ODIN """
from Gaudi.Configuration import appendPostConfigAction
def doIt():
    """
    specific post-config action for (x)GEN-files
    """
    extension = "xgen"
    ext = extension.upper()

    from Configurables import DataOnDemandSvc
    dod  = DataOnDemandSvc()
    from copy import deepcopy
    algs = deepcopy(dod.AlgMap)
    bad  = set()
    for key in algs :
        if     0 <= key.find ( 'Rec'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Raw'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'DAQ'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Trigger' )                  : bad.add ( key )
        elif   0 <= key.find ( 'Phys'    )                  : bad.add ( key )
        elif   0 <= key.find ( 'Prev/'   )                  : bad.add ( key )
        elif   0 <= key.find ( 'Next/'   )                  : bad.add ( key )
        elif   0 <= key.find ( '/MC/'    ) and 'GEN' == ext : bad.add ( key )

    for b in bad :
        del algs[b]

    dod.AlgMap = algs

    from Configurables import EventClockSvc, CondDB
    EventClockSvc(EventTimeDecoder = "FakeEventTime")
    CondDB(IgnoreHeartBeat = True)

appendPostConfigAction( doIt )
doIt()


###################
# Input file definition
###################
# inputFiles = [sys.argv[-1]]

""" Input files """
from GaudiConf import IOHelper
import itertools as it
# base_dir = "/eos/lhcb/lbdt3/user/ldufour/mighty-tracker/2e34_samples_new"
# data = [base_dir + "/{}_MagUp_incl_b_2e33.sim".format(i) for i in it.chain(range(1000, 1099), range(1500, 1599))]
# data = [base_dir + "/{}_MagUp_incl_b_2e34.sim".format(i) for i in range(2000, 2049)]
# data = [base_dir + "/{}_MagUp_bs2phiphi_1p5e34.sim".format(i) for i in range(2500, 2599)]
base_dir = "root://eoslhcb.cern.ch//eos/lhcb/lbdt3/user/ldufour/upgradeII/bs2dspi"
data = [base_dir + "/FullDet_UpgradeII_NoFoil_13264021_4_{}_MagDown.sim".format(i) for i in range(1,51)]
IOHelper('ROOT').inputFiles(data)

###################
## Enable MCHit unpacking for FT
###################
from Configurables import SimConf
SimConf().Detectors=["FT"]

###################
## Init GaudiPython
###################
appMgr = GP.AppMgr()
evt = appMgr.evtsvc()


###################
## Event "loop"
###################

""" This is for the creation of an nTuple """
output_file = ROOT.TFile.Open("15e33-no-foil.root", "RECREATE");
outputTuple =  ROOT.TTree("Hits", "Hits")
import numpy as n

x = n.zeros(1, dtype=float)
y = n.zeros(1, dtype=float)
z = n.zeros(1, dtype=float)
e = n.zeros(1, dtype=float)

outputTuple.Branch("x", x, "x/D")
outputTuple.Branch("y", y, "y/D")
outputTuple.Branch("z", z, "z/D")
outputTuple.Branch("e", e, "e/D")


while True:
    appMgr.run(1)

    if not evt["/Event/MC/FT/Hits"]:
        print "Reached end of input files"
        break

    ft_hits = evt["/Event/MC/FT/Hits"]

    unique_particle_list_per_ft_id = {};

    for ft_hit in ft_hits:

        if ft_hit.entry().Z() > 7810 and ft_hit.entry().Z() < 7840:

            if ft_hit.sensDetID() not in unique_particle_list_per_ft_id:
                unique_particle_list_per_ft_id[ ft_hit.sensDetID() ] = []

            if ft_hit.mcParticle().pt() in unique_particle_list_per_ft_id[ ft_hit.sensDetID() ]:
                continue;

            x[0] = ft_hit.entry().X()
            y[0] = ft_hit.entry().Y()
            z[0] = ft_hit.entry().Z()

            # uniqueness of MCParticle
            unique_particle_list_per_ft_id[ ft_hit.sensDetID() ] += [ ft_hit.mcParticle().pt() ];

            outputTuple.Fill()

    e = e + 1

output_file.cd();
outputTuple.Write();

output_file.Close();
