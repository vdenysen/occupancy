#ifndef occupancy_hpp
#define occupancy_hpp

// ROOT
#include "TROOT.h"
#include "TApplication.h"
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TLegend.h"
// C++
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include <assert.h>
#include <math.h>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <unordered_set>

using namespace std;

class occupancy {
public:
	occupancy(string _in_file, int _scenario);
	~occupancy();
	bool IT_geometry(double _x, double _y);
	bool MT_geometry(double _x, double _y);
	void analysis();
	TH1F * histo_fibre_full_info(string tree_name);
	void histo_fibre_middle(string tree_name);
	TH1F * histo_fibre(string tree_name);
	void histo_outer(string tree_name);
	void histo_inner(string tree_name);
	void histo_bins(string tree_name, int bin_type);
	void verbose(TH1F * _h, double _ratio);
	void verbose(TH2F * _h, double _ratio);
	void same_plot(TH1F * _h1, TH1F * _h2);
	void scifi(TH2F * _h);
	void draw_plots(TH2F * _h, int _case);

private:
	// Scenario number
	int scenario;

	// Layer number in order to run through all of them
	int layer_number = 0;

	map<int, pair<int, int>> layers{
		{1, make_pair(7800, 7850)},
		{2, make_pair(8000, 8080)},
		{3, make_pair(8480, 8540)},
		{4, make_pair(8700, 8750)},
		{5, make_pair(9170, 9230)},
		{6, make_pair(9360, 9450)}
	};

	// Luminosity
	bool is_u2_luminosity;

	// some max parameters to limit the plots
	float int_occ_max;
	float hist_max;

	// Output files
	TFile * data_out = new TFile();
	ofstream out_txt_file;
	string out_file_name;
	string name_prefix;

	unordered_set<int> unique_runs_events;
	int n_events = 0;

	// Dimentions of the IT and/or MT block
	static const int x_block = 530; // mm
	static const int y_block = 200; // mm

	// Dimentions of the Tracking Station
	static const int x_range = 5500; // mm
	static const int y_range = 5000; // mm

	// General bin size <1x1 cm2>
	static constexpr float x_cm_bin_size = 10.;	// mm
	static constexpr float y_cm_bin_size = 10.;	// mm

	// Bin sizes in the Tracking Station
	static constexpr float x_fibre_size = 0.25;	// mm
	// static constexpr float y_fibre_size = 100.; // mm

	// Bin sizes in the MT&IT
	static constexpr float x_strip_size = 0.1;	// mm
	static constexpr float y_strip_size = 100.;	// mm
	//
	// static constexpr float x_pixel1_size = 0.08; // mm
	// static constexpr float y_pixel1_size = 0.08; // mm
	//
	// static constexpr float x_pixel2_size = 0.05; // mm
	// static constexpr float y_pixel2_size = 0.2;  // mm
	//
	static constexpr float x_pixel_size = 0.1;	// mm
	static constexpr float y_pixel_size = 0.3;	// mm
};

#endif
