#include "include/occupancy.hpp"

using namespace std;

namespace {
    void PrintUsage() {
        cerr << "Usage: " << endl;
        cerr << "./occupancy [input file] [Scenario 1] [Scenario 2] ... [Scenarion X]" << endl;
        cerr << "input file: file that is in data/" << endl;
        cerr << "            data/[input file].root" << endl;
        cerr << "Scenario: MT geometry scenarios [1 - 4] (check /config)" << endl;
        cerr << "Examples:" << endl;
        cerr << "./occupancy incl_b/MCtracks_2e33 2 3" << endl;
        cerr << "./occupancy 2e34 4" << endl;
    }
}

int main(int argc,char** argv) {
    
    if(!argv[1]) {
        cerr << "./occupancy [-h] or [--help]" << endl;
        return 1;
    }
    vector<int> scenario;
    gROOT->ProcessLine(".x code/lhcbStyle.cc");
    string inp_file;

    if(string(argv[1]) == "-h" or string(argv[1]) == "--help") {
        PrintUsage();
        return 0;
    } else {
        if(!argv[2]) {
            cerr << "./occupancy [-h] or [--help]" << endl;
            return 1;
        }
        inp_file = argv[1];

        for(int i = 2; i < argc; ++i)
            if(find(scenario.begin(), scenario.end(), atoi(argv[i])) == scenario.end())
                scenario.push_back(atoi(argv[i]));

        for(auto i = begin(scenario); i != end(scenario); ++i)
            occupancy(inp_file, (*i));
    }

    cout << "Done!" << endl;
    return 0;
}
