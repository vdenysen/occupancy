#include "occupancy.hpp"

using namespace std;

occupancy::occupancy(string _in_file, int _scenario)
:scenario(_scenario), out_file_name("occupancy_" + to_string(_scenario)) {
    
    if(_in_file.find("e34") != string::npos) {
        is_u2_luminosity = true;
        int_occ_max = 0.2;
        hist_max = 0.47;
        name_prefix = "1p5e34_";
    } else if(_in_file.find("e33") != string::npos) {
        is_u2_luminosity = false;
        int_occ_max = 0.035;
        hist_max = 0.087;
        name_prefix = "2e33_";
    } else {
        // Some random values
        is_u2_luminosity = false;
        int_occ_max = 0.1;
        hist_max = 0.1;
        name_prefix = "";
    }
    
    
	cout << "You use Scenario " << scenario << " geometry." << endl;

	TFile * data = new TFile();
	TTree * tree = new TTree();

	// ifstream nevents;
	// nevents.open("data/" + _in_file + "_unique_events.txt");
	// if(nevents.is_open())
	// {
	// 	nevents >> n_events;
	// 	nevents.close();
	// }
	// else
	// {
	// 	cout << "###" << endl;
	// 	cout << "# The " + _in_file + "_unique_events.txt file does not exist." << endl;
	// 	cout << "###" << endl;
	// 	exit(EXIT_FAILURE);
	// }

	// Some variable
	double x(0);
	double y(0);
    double z(0);
    double z_origin(0);
    int run(0);
    int event(0);

	vector<pair<double, double>> available_range;
	unsigned long long int n_entries = 0;

	TFile * out_file = new TFile("result/scifi.root", "RECREATE");
	out_file->Close();
	delete out_file;

	out_txt_file.open("result/scenario_" + to_string(scenario) + ".txt");

	for(auto &layer : layers)
	{
		layer_number = layer.first;

		cout << endl;
		cout << "Layer " << layer_number << "." << endl;

		out_txt_file << "Layer " << layer_number << "." << endl;

		data_out = new TFile(("data/" + _in_file + "_" + to_string(layer_number) + "_out.root").data(), "OPEN");

		if(data_out->IsZombie() || data_out->TestBit(TFile::kRecovered)) {
			if(!data->IsOpen()) {
				// Open input file
				data = new TFile(("data/" + _in_file + ".root").data(), "READ");
				tree = (TTree * )data->Get("Hits");

				// Linking variables
				tree->SetBranchAddress("x", &x);
				tree->SetBranchAddress("y", &y);
				tree->SetBranchAddress("z", &z);
				tree->SetBranchAddress("z_orgn", &z_origin);
				tree->SetBranchAddress("run", &run);
				tree->SetBranchAddress("event", &event);

				n_entries = tree->GetEntries();

				ifstream available_range_file;
				available_range_file.open("data/available_range.txt");
				if(available_range_file.is_open()) {
					double x_min, x_max;
					while(available_range_file.good()) {
						available_range_file >> x_min >> x_max;
						available_range.push_back(make_pair(x_min, x_max));
					}
					available_range_file.close();
				}
			}

			data_out = new TFile(("data/" + _in_file + "_" + to_string(layer_number) + "_out.root").data(), "RECREATE");

			TNtuple * full_ntuple = new TNtuple("without_spikes", "", "x:y:z_origin:run:event");
			TNtuple * woMT_ntuple = new TNtuple("outside_MT",     "", "x:y:z_origin:run:event");
			TNtuple * woIT_ntuple = new TNtuple("outside_IT",     "", "x:y:z_origin:run:event");
			TNtuple * MT_ntuple   = new TNtuple("inside_MT",      "", "x:y:z_origin:run:event");
			TNtuple * IT_ntuple   = new TNtuple("inside_IT",      "", "x:y:z_origin:run:event");
			// TODO: Make a new tuple with the unique_runs_events information to read it from while pre-processed
			// 		 Replace the txt file writing / reading

			// Main loop
			for(unsigned int i = 0; i < n_entries; ++i) {
				tree->GetEntry(i);

				if(z < (layer.second).first || z > (layer.second).second)
		        	continue;

				unique_runs_events.insert(run * 100 + event);

		    	// if(z_orgn > -100. && z_orgn < 95)
				for(auto &k : available_range) {
					if(x > k.first and x < k.second) {
						full_ntuple->Fill(x, y, z_origin, run, event);

                        if(!(abs(x) <= 100. and abs(y) <= 100.)) {	// cut out the beam pipe
							if(IT_geometry(x,y))
								IT_ntuple->Fill(x, y, z_origin, run, event);
							else {
								woIT_ntuple->Fill(x, y, z_origin, run, event);

								if(MT_geometry(x, y))
									MT_ntuple->Fill(x, y, z_origin, run, event);
								else
									woMT_ntuple->Fill(x, y, z_origin, run, event);
							}
						}
						break;
					}
				}
			}

			n_events = unique_runs_events.size();
			// cout << n_events << endl;

			// TODO: Replace the writing part
			ofstream nevents;
			nevents.open("data/" + _in_file + "_" + to_string(layer_number) + "_unique_events.txt", ofstream::out | ofstream::app);
			if(nevents.is_open()) {
				nevents << n_events << endl;
				for(const int& run_event: unique_runs_events)
					nevents << run_event << endl;
				nevents.close();
			} else {
				cerr << "Could not create the data/" + _in_file + "_" + to_string(layer_number) + "_unique_events.txt file." << endl;
				exit(EXIT_FAILURE);
			}

			full_ntuple->Write();
			woMT_ntuple->Write();
			woIT_ntuple->Write();
			MT_ntuple->Write();
			IT_ntuple->Write();

		} else {
			// TODO: Replace the reading part
			ifstream nevents;
			nevents.open("data/" + _in_file + "_" + to_string(layer_number) + "_unique_events.txt");
			if(nevents.is_open()) {
				nevents >> n_events;
				int tmp_run_event = -1;
				while(nevents.good()) {
					tmp_run_event = -1;
					nevents >> tmp_run_event;
					if(tmp_run_event != -1)
						unique_runs_events.insert(tmp_run_event);
				}
				nevents.close();
			} else {
				cerr << "The data/" + _in_file + "_" + to_string(layer_number) + "_unique_events.txt file does not exist." << endl;
				exit(EXIT_FAILURE);
			}
		}

		analysis();
		data_out->Close();
	}
	cout << endl;
	cout << "Analysis successfully done!" << endl;

	data->Close();
	delete data;
}

occupancy::~occupancy() {
	out_txt_file.close();
}

bool occupancy::IT_geometry(double _x, double _y) {
	if(scenario == 1 || scenario == 2 || scenario == 3) {
		if(abs(_x) <= x_block and abs(_y) <= y_block)
		return true;
    } else if(abs(_x) <= x_block and abs(_y) <= 1.5 * y_block) {
		return true;
	}
	return false;
}

bool occupancy::MT_geometry(double _x, double _y) {
	vector<pair<double, double>> block_center;
	double x_max(0.);
	double y_max(0.);

	ifstream mt_geometry;
	// ios_base::iostate exceptionMask = mt_geometry.exceptions() | std::ios::failbit;
	// mt_geometry.exceptions(exceptionMask);

	mt_geometry.open("config/MT_" + to_string(scenario) + ".txt");
	if(mt_geometry.is_open()) {
		double tmp_x, tmp_y;
		while(mt_geometry.good()) {
			mt_geometry >> tmp_x >> tmp_y;
			block_center.push_back(make_pair(tmp_x, tmp_y));
		}
		mt_geometry.close();
		x_max = block_center.back().first * x_block;
		y_max = block_center.back().second * y_block;
		block_center.pop_back();
		block_center.pop_back();
	}

	if(abs(_x) <= x_max and abs(_y) <= y_max) {
		for(auto &i : block_center) {
			if(abs(_x - i.first * x_block) <= x_block / 2. && abs(_y - i.second * y_block) <= y_block / 2.)
				return true;
		}
	}
	return false;
}

void occupancy::analysis() {
	// histo_fibre_middle("without_spikes");
	// histo_fibre_middle("outside_IT");
	// histo_fibre_middle("outside_MT");

	TH1F * h1 = histo_fibre("without_spikes");
	TH1F * h2 = histo_fibre("outside_MT");
	same_plot(h1,h2);
	h2 = histo_fibre("outside_IT");
	same_plot(h1,h2);

	delete h1;
	delete h2;
	// th1f: done

	histo_outer("without_spikes");
	histo_outer("outside_MT");
	histo_outer("outside_IT");

	histo_inner("inside_MT");
	histo_inner("inside_IT");

	histo_bins("inside_MT", 0);
	histo_bins("inside_IT", 0);

	histo_bins("inside_MT", 1);
	histo_bins("inside_IT", 1);
	// th2f: done
}

TH1F * occupancy::histo_fibre_full_info(string tree_name) {
	TTree * tree = (TTree * )data_out->Get(tree_name.data());
	string plot_cut = "x>>(" + to_string(x_range/x_fibre_size) + ", -" + to_string(x_range/2) + ", " + to_string(x_range/2) + ")";

	int run = 0;
	int event = 0;

	TH1F * h_max = new TH1F("max_", "", 100, 0, 50);
	h_max->GetXaxis()->SetTitle("Maximum occupancy [1 / fibre event]");
	TH1F * h_average = new TH1F("average_", "", 25000, 0, 0.25);
	h_average->GetXaxis()->SetTitle("Average occupancy [1 / fibre event]");

	for(const int& run_event: unique_runs_events) {
		run = run_event / 100;
		event = run_event % 100;
		string cut("run == " + to_string(run) + " && event == " + to_string(event));
		// cout << cut << endl;
		tree->Draw(plot_cut.data(), cut.data());
		TH1F * h = (TH1F *)gPad->GetPrimitive("");
		h->Scale(0.5);

		h_max->Fill(h->GetBinContent(h->GetMaximumBin()));
		h_average->Fill(h->Integral()/h->GetNbinsX());
	}
	tree->Draw(plot_cut.data(), "");
	TH1F * h = (TH1F *)gPad->GetPrimitive("");
	h->SetName("x");
	gPad->GetCanvas()->Close();

	if(tree_name.compare("outside_MT") == 0)
		h->SetName("x_wo_MT");
	else if(tree_name.compare("outside_IT") == 0)
		h->SetName("x_wo_IT");

	TFile * out_file = new TFile("result/scifi.root", "UPDATE");

	TCanvas * c = new TCanvas("c", "c", 1280, 720);
	c->SetRightMargin(0.05);

	string histo_name((string)h_max->GetName() + (string)h->GetName());
	h_max->SetName(histo_name.data());
	h_max->Draw("HIST");
	histo_name += "_" + to_string(layer_number);
	h_max->Write(histo_name.data());
	histo_name = "result/" + name_prefix + (string)h_max->GetName() + "_" + to_string(scenario) + "_" + to_string(layer_number) + ".png";
	c->SaveAs(histo_name.data());

	histo_name = (string)h_average->GetName() + (string)h->GetName();
	h_average->SetName(histo_name.data());
	h_average->Draw("HIST");
	histo_name += "_" + to_string(layer_number);
	h_average->Write(histo_name.data());
	histo_name = "result/" + name_prefix + (string)h_average->GetName() + "_" + to_string(scenario) + "_" + to_string(layer_number) + ".png";
	c->SaveAs(histo_name.data());

	out_file->Close();
	delete out_file;

	h->GetXaxis()->SetTitle("x [mm]");
	h->GetYaxis()->SetTitle("Occupancy [1 / fibre event]");

	histo_name = h->GetName();
	float max = h_max->GetMean();
	float std_max = h_max->GetRMS();
	float average = h_average->GetMean();
	float std_average = h_average->GetRMS();
	cout << endl;
	cout << "Occupancy in the " << histo_name << " ." << endl;
	cout << "Max. value: "
	     << max << " ± " << std_max
	     << ", Average: "
	     << average << " ± " << std_average << " ." << endl;

	out_txt_file << "Occupancy in the " << histo_name << " .\n"
                 << "Max. value: "
                 << max << " ± " << std_max
                 << ", Average: "
                 << average << " ± " << std_average << " .\n";

	h->Scale(0.5 * n_events);
	h->Rebin(40.);
	h->Scale(0.025);

	h->Draw("HIST");
	h->GetYaxis()->SetRangeUser(0., int_occ_max);
	histo_name = "result/" + name_prefix + (string)h->GetName() + "_" + to_string(scenario) + "_" + to_string(layer_number) + ".png";
	c->SaveAs(histo_name.data());

	c->Close();
	delete c;
	delete tree;
	return h;
}

// TODO: Need to be rewriten in a normal way
//		 Dont have time for this yet
void occupancy::histo_fibre_middle(string tree_name) {
	TTree * tree = (TTree * )data_out->Get(tree_name.data());

	int run = 0;
	int event = 0;

	TH1F * h_fibres = new TH1F("fired_fibres_x", "", 100000, 0., 1.);
	h_fibres->GetXaxis()->SetTitle("[1 / fibre event]");
	h_fibres->GetYaxis()->SetTitle("Number of events");

	for(const int& run_event: unique_runs_events) {
		run = run_event / 100;
		event = run_event % 100;
		string cut("run == " + to_string(run) + " && event == " + to_string(event));
		string plot_cut("");

		int number_of_fibres = 0;
		int number_of_bins = 0;
		if(tree_name.compare("outside_MT") == 0) {
			plot_cut = "y:x>>(" + to_string(x_range/x_fibre_size) + ", -" + to_string(x_range/2) + ", " + to_string(x_range/2) +
			", " + to_string(2) + ", -" + to_string(y_range/2) + ", " + to_string(y_range/2) + ")";
			tree->Draw(plot_cut.data(), cut.data(), "colz");
			TH2F * h = (TH2F *)gPad->GetPrimitive("");
			for(int i = 0; i <= h->GetNbinsX(); ++i)
				for(int j = 0; j <= 2; ++j)
					if(h->GetBinContent(i,j) > 0.)
						++number_of_fibres;
			number_of_bins = 2 * h->GetNbinsX();
		} else if(tree_name.compare("without_spikes") == 0) {
			float x_scifi = 144.25;
			string tmp_cut(cut + " && ((x >= 134.25) || (x <= -134.25))");
			plot_cut = "y:x>>(" + to_string(2*x_scifi/x_fibre_size) + ", -" + to_string(x_scifi) + ", " + to_string(x_scifi) +
			", " + to_string(2) + ", -" + to_string(y_range/2) + ", " + to_string(y_range/2) + ")";
			tree->Draw(plot_cut.data(), tmp_cut.data(), "colz");
			TH2F * h = (TH2F *)gPad->GetPrimitive("");
			for(int i = 0; i <= h->GetNbinsX(); ++i)
				for(int j = 0; j <= 2; ++j)
					if(h->GetBinContent(i,j) > 0.)
						++number_of_fibres;
			number_of_bins = 160;
		} else if(tree_name.compare("outside_IT") == 0) {
			float x_it = 544.25;
			string tmp_cut(cut + " && ((x >= 534.25) || (x <= -534.25))");
			plot_cut = "y:x>>(" + to_string(2*x_it/x_fibre_size) + ", -" + to_string(x_it) + ", " + to_string(x_it) +
			", " + to_string(2) + ", -" + to_string(y_range/2) + ", " + to_string(y_range/2) + ")";
			tree->Draw(plot_cut.data(), tmp_cut.data(), "colz");
			TH2F * h = (TH2F *)gPad->GetPrimitive("");
			for(int i = 0; i <= h->GetNbinsX(); ++i)
				for(int j = 0; j <= 2; ++j)
					if(h->GetBinContent(i,j) > 0.)
						++number_of_fibres;
			number_of_bins = 160;
		}

		h_fibres->Fill(1.0 * number_of_fibres / number_of_bins);
	}

	if(tree_name.compare("outside_MT") == 0)
		h_fibres->SetName("fired_fibres_x_wo_MT");
	else if(tree_name.compare("outside_IT") == 0)
		h_fibres->SetName("fired_fibres_x_wo_IT");

	TFile * out_file = new TFile("result/scifi.root", "UPDATE");

	TCanvas * c = new TCanvas("c", "c", 1280, 720);
	c->SetRightMargin(0.05);

	h_fibres->Draw("HIST");
	string histo_name((string)h_fibres->GetName() + "_" + to_string(layer_number));
	h_fibres->Write(histo_name.data());
	histo_name = "result/" + name_prefix + (string)h_fibres->GetName() + "_" + to_string(scenario) + "_" + to_string(layer_number) + ".png";
	c->SaveAs(histo_name.data());

	out_file->Close();
	delete out_file;

	c->Close();
	delete c;
	delete tree;
}

TH1F * occupancy::histo_fibre(string tree_name) {
	TTree * tree = (TTree * )data_out->Get(tree_name.data());
	string plot_cut = "x>>(" + to_string(x_range/x_fibre_size) + ", -" + to_string(x_range/2) + ", " + to_string(x_range/2) + ")";
	// tree->Draw(plot_cut, "y <= 0");
	tree->Draw(plot_cut.data(), "");
	TH1F * h = (TH1F *)gPad->GetPrimitive("");
	gPad->GetCanvas()->Close();

	if(tree_name.compare("without_spikes") == 0)
		h->SetName("x");
	else if(tree_name.compare("outside_MT") == 0)
		h->SetName("x_wo_MT");
	else if(tree_name.compare("outside_IT") == 0)
		h->SetName("x_wo_IT");

	h->GetXaxis()->SetTitle("x [mm]");
	h->GetYaxis()->SetTitle("Occupancy [1 / fibre event]");

	double ratio = 1. / (2. * n_events);

	verbose(h, ratio);

	h->Scale(ratio);

	TCanvas * c = new TCanvas("c", "c", 1280, 720);
	c->SetRightMargin(0.05);

	h->Rebin(40.);
	h->Scale(0.025);

	h->Draw("HIST");
	h->GetYaxis()->SetRangeUser(0., int_occ_max);
	string histo_name("result/" + name_prefix + (string)h->GetName() + "_" + to_string(scenario) + "_" + to_string(layer_number) + ".png");
	c->SaveAs(histo_name.data());

	c->Close();
	delete c;
	delete tree;
	return h;
}

void occupancy::histo_outer(string tree_name) {
	TTree * tree = (TTree * )data_out->Get(tree_name.data());
	string plot_cut = "y:x>>(" + to_string(x_range/x_cm_bin_size) + ", -" + to_string(x_range/2) + ", " + to_string(x_range/2) +
	", " + to_string(y_range/y_cm_bin_size) + ", -" + to_string(y_range/2) + ", " + to_string(y_range/2) + ")";
	tree->Draw(plot_cut.data(), "", "colz");
	TH2F * h = (TH2F *)gPad->GetPrimitive("");
	gPad->GetCanvas()->Close();

	if(tree_name.compare("without_spikes") == 0)
		h->SetName("x_y");
	else if(tree_name.compare("outside_MT") == 0)
		h->SetName("x_y_wo_MT");
	else if(tree_name.compare("outside_IT") == 0)
		h->SetName("x_y_wo_IT");

	h->GetXaxis()->SetTitle("x [mm]");
	h->GetYaxis()->SetTitle("y [mm]");
	h->GetZaxis()->SetTitle("Hit density [1 / cm^{2} event]");

	double ratio = 1. / n_events;

	h->Scale(ratio);

	string histo_name(h->GetName());

	TCanvas * c = new TCanvas("c", "c", 1280, 720);
	h->SetMaximum(hist_max);
	h->Draw("COLZ");

	TBox * b = new TBox(-100., -100., 100., 100.);
	b->SetFillColor(kWhite);
	b->Draw();
	c->Update();
	histo_name = "result/" + name_prefix + histo_name + "_" + to_string(scenario) + "_" + to_string(layer_number) + ".png";
	c->Print(histo_name.data());

	// h->Scale(1./ratio);
	// verbose(h, ratio);
	// h->Scale(ratio);

	histo_name = h->GetName();
	if(histo_name.find("IT") == string::npos && histo_name.find("MT") == string::npos) {
		h->GetXaxis()->SetRangeUser(-900., 900.);
		h->GetYaxis()->SetRangeUser(-600., 600.);
		h->SetMaximum(hist_max);
		h->Draw("COLZ");
		b->Draw();
		c->Update();
		histo_name = "result/" + name_prefix + histo_name + "_" + to_string(scenario) + "_" + to_string(layer_number) + + "_zoom.png";
		c->Print(histo_name.data());
		h->GetXaxis()->SetRangeUser(-x_range/2., x_range/2.);
		h->GetYaxis()->SetRangeUser(-y_range/2., y_range/2.);
	}

	c->Close();
	delete b;
	delete c;
	scifi(h);
	delete h;
	delete tree;
}

void occupancy::histo_inner(string tree_name) {
	TTree * tree = (TTree * )data_out->Get(tree_name.data());
	if(tree_name.compare("inside_MT") == 0) {
		string plot_cut = "y:x>>(" + to_string(4400/x_cm_bin_size) + ", -" + to_string(2200) + ", " + to_string(2200) +
		", " + to_string(2600/y_cm_bin_size) + ", -" + to_string(1300) + ", " + to_string(1300) + ")";
		tree->Draw(plot_cut.data(), "", "colz");
	} else if(tree_name.compare("inside_IT") == 0) {
		string plot_cut = "y:x>>(" + to_string(1200/x_cm_bin_size) + ", -" + to_string(600) + ", " + to_string(600) + ", " +
		to_string(900/y_cm_bin_size) + ", -" + to_string(450) + ", " + to_string(450) + ")";
		tree->Draw(plot_cut.data(), "", "colz");
	}

	TH2F * h = (TH2F *)gPad->GetPrimitive("");
	gPad->GetCanvas()->Close();

	if(tree_name.compare("inside_MT") == 0)
		h->SetName("x_y_MT");
	else if(tree_name.compare("inside_IT") == 0)
		h->SetName("x_y_IT");

	h->GetXaxis()->SetTitle("x [mm]");
	h->GetYaxis()->SetTitle("y [mm]");
	h->GetZaxis()->SetTitle("Hit density [1 / cm^{2} event]");

	double ratio = 1. / n_events;

	h->Scale(ratio);

	string histo_name(h->GetName());

	TCanvas * c = new TCanvas("c", "c", 1280, 720);
	h->SetMaximum(hist_max);
	h->Draw("COLZ");

	TBox * b = new TBox(-100., -100., 100., 100.);
	b->SetFillColor(kWhite);
	b->Draw();
	c->Update();
	histo_name = "result/" + name_prefix + histo_name + "_" + to_string(scenario) + "_" + to_string(layer_number) + ".png";
	c->Print(histo_name.data());

	// h->Scale(1./ratio);
	// verbose(h, ratio);

	c->Close();
	delete b;
	delete c;
	delete h;
	delete tree;
}

void occupancy::histo_bins(string tree_name, int bin_type) {
	TTree * tree = (TTree * )data_out->Get(tree_name.data());
	if(tree_name.compare("inside_MT") == 0) {
		string plot_cut = "";
		if(bin_type == 0)
			plot_cut = "y:x>>(" + to_string(440/x_pixel_size) + ", -" + to_string(2200) + ", " + to_string(2200) + ", " + to_string(259.8/y_pixel_size) + ", -" + to_string(1299) + ", " + to_string(1299) + ")";
		else if(bin_type == 1)
			plot_cut = "y:x>>(" + to_string(440/x_strip_size) + ", -" + to_string(2200) + ", " + to_string(2200) + ", " + to_string(2600/y_strip_size) + ", -" + to_string(1300) + ", " + to_string(1300) + ")";
		tree->Draw(plot_cut.data(), "", "colz");
	} else if(tree_name.compare("inside_IT") == 0) {
		string plot_cut = "";
		if(bin_type == 0)
			plot_cut = "y:x>>(" + to_string(120/x_pixel_size) + ", -" + to_string(600) + ", " + to_string(600) + ", " + to_string(90/y_pixel_size) + ", -" + to_string(450) + ", " + to_string(450) + ")";
		else if(bin_type == 1)
			plot_cut = "y:x>>(" + to_string(120/x_strip_size) + ", -" + to_string(600) + ", " + to_string(600) + ", " + to_string(900/y_strip_size) + ", -" + to_string(450) + ", " + to_string(450) + ")";
		tree->Draw(plot_cut.data(), "", "colz");
	}

	TH2F * h = (TH2F *)gPad->GetPrimitive("");
	gPad->GetCanvas()->Close();

	if(tree_name.compare("inside_MT") == 0) {
		if(bin_type == 0)
			h->SetName("x_y_MT_pixel");
		else if(bin_type == 1)
			h->SetName("x_y_MT_strip");
	} else if(tree_name.compare("inside_IT") == 0) {
		if(bin_type == 0)
			h->SetName("x_y_IT_pixel");
		else if(bin_type == 1)
			h->SetName("x_y_IT_strip");
	}

	h->GetXaxis()->SetTitle("x [mm]");
	h->GetYaxis()->SetTitle("y [mm]");
	if(bin_type == 0)
		h->GetZaxis()->SetTitle("Occupancy [1 / pixel event]");
	else if(bin_type == 1)
		h->GetZaxis()->SetTitle("Occupancy [1 / strip event]");

	double ratio = 1. / n_events;

	h->Scale(ratio);
	if(bin_type == 0)
		h->Scale(0.01);
	else if(bin_type == 1)
		h->Scale(0.1);

	if(is_u2_luminosity) {
		if(tree_name.compare("inside_MT") == 0) {
			if(bin_type == 0)
				h->SetMaximum(5.e-5);
			else if(bin_type == 1)
				h->SetMaximum(0.01);
		} else if(tree_name.compare("inside_IT") == 0) {
			if(bin_type == 0)
				h->SetMaximum(1.8e-4);
			else if(bin_type == 1)
				h->SetMaximum(0.05);
		}
	} else if(tree_name.compare("inside_IT") == 0) {
			if(bin_type == 0)
				h->SetMaximum(3.5e-5);
			else if(bin_type == 1)
				h->SetMaximum(0.008);
    }

	string histo_name(h->GetName());

	TCanvas * c = new TCanvas("c", "c", 1280, 720);

	h->Draw("COLZ");

	gROOT->ProcessLine(".x code/beamhole.cc");
	c->Update();
	histo_name = "result/" + name_prefix + histo_name + "_" + to_string(scenario) + "_" + to_string(layer_number) + ".png";
	c->Print(histo_name.data());

	// h->Scale(1./ratio);
	// verbose(h, ratio);

	c->Close();
	delete c;
	delete h;
	delete tree;
}

void occupancy::verbose(TH1F * _h, double _ratio) {
	double max = _h->GetBinContent(_h->GetMaximumBin());
	double average = _h->Integral()/_h->GetNbinsX();

	double std_max = sqrt(max);
	double std_average = sqrt(average);

	max *= _ratio;
	average *= _ratio;
	std_max *= _ratio;
	std_average *= _ratio;

	string histo_name(_h->GetName());
	cout << endl;
	cout << "Occupancy in the " << histo_name << " ." << endl;
	cout << "Max. value: "
	     << max << " ± " << std_max
	     << ", Average: "
	     << average << " ± " << std_average << " ." << endl;

	out_txt_file << "Occupancy in the " << histo_name << " .\n"
                 << "Max. value: "
                 << max << " ± " << std_max
                 << ", Average: "
                 << average << " ± " << std_average << " .\n";
}

void occupancy::verbose(TH2F * _h, double _ratio) {

	int n_bins = 0;

	string histo_name(_h->GetName());
    
    // MAGIC NUMBERS!!! :)

	if(histo_name.compare("x_y") == 0) {
		n_bins = 265*243 - 20*20;
	}
	else if(histo_name.compare("x_y_wo_IT") == 0) {
		if(scenario == 1 || scenario == 2 || scenario == 3)
			n_bins = 265*243 - 53*20*4;
		else
			n_bins = 265*243 - 53*20*6;
	}
	else if(histo_name.compare("x_y_wo_MT") == 0) {
		if(scenario == 1)
			n_bins = 265*243 - 53*20*12;
		else if(scenario == 2)
			n_bins = 265*243 - 53*20*14;
		else if(scenario == 3)
			n_bins = 265*243 - 53*20*16;
		else
			n_bins = 265*243 - 53*20*28;
	} else if(histo_name.compare("x_y_IT") == 0) {
		if(scenario == 1 || scenario == 2 || scenario == 3)
			n_bins = 53*20*4 - 20*20;
		else
			n_bins = 53*20*6 - 20*20;
	} else if(histo_name.compare("x_y_MT") == 0) {
		if(scenario == 1)
			n_bins = 53*20*8;
		else if(scenario == 2)
			n_bins = 53*20*10;
		else if(scenario == 3)
			n_bins = 53*20*12;
		else
			n_bins = 53*20*22;
	} else if(histo_name.compare("x_y_MT_pixel") == 0) {
		n_bins = 3533334;
		if(scenario == 1)
			n_bins *= 8;
		else if(scenario == 2)
			n_bins *= 10;
		else if(scenario == 3)
			n_bins *= 12;
		else
			n_bins *= 22;
	} else if(histo_name.compare("x_y_IT_pixel") == 0) {
		n_bins = 3200000;
		if(scenario == 1 || scenario == 2 || scenario == 3)
			n_bins *= 4;
		else
			n_bins *= 6;
	} else if(histo_name.compare("x_y_MT_strip") == 0) {
		n_bins = 10600;
		if(scenario == 1)
			n_bins *= 8;
		else if(scenario == 2)
			n_bins *= 10;
		else if(scenario == 3)
			n_bins *= 12;
		else
			n_bins *= 22;
	} else if(histo_name.compare("x_y_IT_strip") == 0) {
		n_bins = 9600;
		if(scenario == 1 || scenario == 2 || scenario == 3)
			n_bins *= 4;
		else
			n_bins *= 6;
	}

	double average = _h->Integral()/n_bins;
	double std_average = sqrt(average);

	double max = _h->GetBinContent(_h->GetMaximumBin());
	double std_max = sqrt(max);

	max *= _ratio;
	average *= _ratio;
	std_max *= _ratio;
	std_average *= _ratio;

	cout << endl;
	cout << "Occupancy in the " << histo_name << " ." << endl;
	cout << "Max. value: "
	     << max << " ± " << std_max
	     << ", Average: "
	     << average << " ± " << std_average << " ." << endl;

	out_txt_file << "Occupancy in the " << histo_name << " .\n"
                 << "Max. value: "
                 << max << " ± " << std_max
                 << ", Average: "
                 << average << " ± " << std_average << " .\n";
}


void occupancy::same_plot(TH1F * _h1, TH1F * _h2) {
	TCanvas * c = new TCanvas("c", "c", 1280, 720);

	_h2->SetLineColor(kRed);

	_h1->Draw("HIST");
	_h2->Draw("HIST+SAME");

	_h1->GetYaxis()->SetRangeUser(0., int_occ_max);
	_h2->GetYaxis()->SetRangeUser(0., int_occ_max);

	TLegend * legend = new TLegend(0.65,0.65,0.92,0.92);
	legend->SetFillColor(0);
	legend->SetTextSize(0.055);
	string h2_name_tmp(_h2->GetName());
	if(h2_name_tmp.find("MT") != string::npos) {
		legend->AddEntry(_h1,"SciFi","L");
		legend->AddEntry(_h2,"SciFi with MT","L");
	} else if(h2_name_tmp.find("IT") != string::npos) {
		legend->AddEntry(_h1,"SciFi","L");
		legend->AddEntry(_h2,"SciFi with IT","L");
	}
	legend->Draw();
	c->SetRightMargin(0.05);
	c->Update();

	TFile * out_file = new TFile("result/scifi.root", "UPDATE");
	c->Write((h2_name_tmp + "_comp_" + to_string(layer_number)).data());
	string name_tmp(_h1->GetName());
	name_tmp += "_" + to_string(layer_number);
	_h1->Write(name_tmp.data());
	name_tmp = _h2->GetName();
	name_tmp += "_" + to_string(layer_number);
	_h2->Write(name_tmp.data());

	out_file->Close();
	delete out_file;

	// TFile * out_file = new TFile(name_root.data(), "UPDATE");
	// h2_name_tmp += "_comp";
	// c->Write(h2_name_tmp);
	//
	// out_file->Close();
	// delete out_file;

	h2_name_tmp = "result/" + name_prefix + h2_name_tmp + "_" + to_string(scenario) + "_" + to_string(layer_number) + ".png";
	c->SaveAs(h2_name_tmp.data());
	// h2_name_tmp.ReplaceAll(".png", ".C");
	// c->SaveAs(h2_name_tmp.data());
	c->Close();
	delete legend;
	delete c;
}

void occupancy::scifi(TH2F * _h) {
	TFile * out_file = new TFile("result/scifi.root", "UPDATE");
	string name(_h->GetName());
	_h->SetName((name + "_" + to_string(layer_number)).data());
	_h->Write();

	int n_bins_x = _h->GetNbinsX();
	int n_bins_y = _h->GetNbinsY();
	for(int i = 0; i < n_bins_x; ++i) {
		for(int j = n_bins_y/2; j < n_bins_y; ++j) {
			_h->SetBinContent(i,j, _h->Integral(i,i, j,(n_bins_y-1)));
		}
		for(int j = n_bins_y/2 - 1.; j >= 0; --j) {
			_h->SetBinContent(i,j, _h->Integral(i,i, 0,j));
		}
	}

	_h->GetZaxis()->SetTitle("Occupancy [1 / fibre event]");
	_h->Scale(0.025);
	_h->SetName((name + "_int_occ_" + to_string(layer_number)).data());
	_h->Write();
	if(name.find("MT") != string::npos || name.find("IT") != string::npos)
		draw_plots(_h, 2);
	else {
		draw_plots(_h, 0);
		draw_plots(_h, 1);
	}

	out_file->Close();
	delete out_file;
}

void occupancy::draw_plots(TH2F * _h, int _case) {
	string name(_h->GetName());
	TCanvas * c = new TCanvas("c", "c", 1280, 720);

	switch (_case) {
		case 0: {
			_h->SetMaximum(int_occ_max);
			_h->Draw("COLZ");
			TBox * b = new TBox(-100., -100., 100., 100.);
			b->SetFillColor(kWhite);
			b->Draw();
			c->Update();
        } break;
		case 1: {
			name += "_zoom";
			_h->SetMaximum(int_occ_max);
			_h->GetXaxis()->SetRangeUser(-900., 900.);
			_h->GetYaxis()->SetRangeUser(-600., 600.);
			_h->Draw("COLZ");
			// gROOT->ProcessLine(".x code/line.cc");
			TBox * b = new TBox(-100., -100., 100., 100.);
			b->SetFillColor(kWhite);
			b->Draw();
			c->Update();
		} break;
		case 2: {
			_h->Draw("COLZ");
			if(name.find("MT") != string::npos)
				gROOT->ProcessLine(".x code/box_MT.cc");
			else if(name.find("IT") != string::npos)
				gROOT->ProcessLine(".x code/box_IT.cc");
			c->Update();
		} break;
	}
	name = "result/" + name_prefix + name + ".png";
	c->SaveAs(name.data());
	c->Close();
	delete c;
}
